/* Code By Webdevtrick ( https://webdevtrick.com ) */
// include("https://cdn.jsdelivr.net/npm/chart.js");
(function(document) {
	'use strict';

	var TableFilter = (function(Arr) {

		var _input;

		function _onInputEvent(e) {
			_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) {
				Arr.forEach.call(table.tBodies, function(tbody) {
					Arr.forEach.call(tbody.rows, _filter);
				});
			});
		}

		function _filter(row) {
			var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		}

		return {
			init: function() {
				var inputs = document.getElementsByClassName('light-table-filter');
				Arr.forEach.call(inputs, function(input) {
					input.oninput = _onInputEvent;
				});
			}
		};
	})(Array.prototype);

	document.addEventListener('readystatechange', function() {
		if (document.readyState === 'complete') {
			TableFilter.init();
		}
	});

})(document);


const labels = [
    '1','8','15','22','29','31'];

  const data = {
    labels: labels,
    datasets: [{
      label: 'Connexion en janvier',
      backgroundColor: 'rgb(255, 99, 132)',
      borderColor: 'rgb(255, 99, 132)',
      data: [0, 10, 5, 2, 30, 13],
    }]
  };

  const config = {
    type: 'line',
    data: data,
    options: {}
  };

  const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );


  const myChart2 = new Chart(
    document.getElementById('myChart2'),
    config
  );

  const myChart3 = new Chart(
    document.getElementById('myChart3'),
    config
  );
  const myChart4 = new Chart(
    document.getElementById('myChart4'),
    config
  );

function deleteEnt(id) {
	$.ajax({
		url: "Admin/DeleteEnt/"+id,
		type: "POST",
		success:function () {
			deleteRow("rowEnt"+id)
		},
		error:function(err) {
			alert("something went wrong")
		}
	});
}

function deleteEtu(id) {
	$.ajax({
		url: "Admin/DeleteEtu/"+id,
		type: "POST",
		success:function () {
			deleteRow("rowEtu"+id)
		},
		error:function() {
			console.log("non")
			alert("Cet utilisateur est un administrateur, vous ne pouvez pas supprimer les administrateurs");
		}
	});
}

function deleteComp(id) {
	$.ajax({
		url: "Admin/DeleteComp/"+id,
		type: "POST",
		success:function () {
			deleteRow("rowComp"+id)
		},
		error:function(err) {
			alert("something went wrong")
		}
	});
}
  

function deleteRow(id) {
	let row = document.getElementById(id);
	let parent = row.parentNode;
	parent.removeChild(row);
}


function sauverEnt() {
	let table = document.getElementById("table-ent")
	let length = table.rows['length'];
	let BlackListData = new Array();
	for (let i = 1; i < length; i++) {
		BlackListData.push([table.rows[i].cells[2].children[0].id,table.rows[i].cells[2].children[0].checked]);
	}

	console.log(BlackListData);
	$.ajax({
		url: "Admin/SauverEnt",
		type: "POST",
		data: {BlackListData : JSON.stringify(BlackListData)},
		success:function () {
			alert("Everything went well");
		},
		error:function(err) {
			alert("Something went wrong");
		}
	});
}

function sauverEtu() {
	let table = document.getElementById("table-etu")
	let length = table.rows['length'];
	let AdminData = new Array();
	for (let i = 1; i < length; i++) {
		AdminData.push([table.rows[i].cells[3].children[0].id,table.rows[i].cells[3].children[0].checked]);
	}
	
	console.log(AdminData);
	$.ajax({
		url: "Admin/SauverEtu",
		type: "POST",
		data: {AdminData : JSON.stringify(AdminData)},
		success:function () {
			alert("Everything went well");
		},
		error:function(err) {
			alert("Something went wrong");
		}
	});
}
