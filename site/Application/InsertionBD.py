from datetime import datetime
from .BDclass import *
from hashlib import sha256


def jeu_dessai():
    db.drop_all()
    db.create_all()

    insertion_Competence()
    insertion_User()
    insertion_Comp()

    db.session.commit()

    print(Ent.query.all())
    print(User.query.all())
    print(Competence.query.all())
    # print(Lien_cpt_ent.query.all())
    # print(Proposition.query.all())


def insertion_User():
    """function to populate some user in database need database.create_all() before excute this"""
    m = sha256()
    m.update("password".encode())
    password = m.hexdigest()
    db.session.add(User(nom="HENRIETTE", prenom="Jules", administrateur=False, password=password,
                        dateNaissance=datetime.fromisoformat('2003-09-08'), numEtu=2,
                        _competences=[get_Cpt_by_name("PHP")]))
    db.session.add(User(nom="TUMAR", prenom="Berfan", administrateur=False, password=password,
                        dateNaissance=datetime.fromisoformat('2003-09-08'), numEtu=3))
    db.session.add(User(nom="FAUBOURG", prenom="Luca", administrateur=True, password=password,
                        dateNaissance=datetime.fromisoformat('2003-09-08'), numEtu=1))
    db.session.add(User(nom="LANGLAIS", prenom="Alexis", administrateur=False, password=password,
                        dateNaissance=datetime.fromisoformat('2003-09-08'), numEtu=4))
    db.session.add(User(nom="RIGNON", prenom="Bastien", administrateur=True, password=password,
                        dateNaissance=datetime.fromisoformat('2003-09-08'), numEtu=5))
    db.session.add(User(nom="BOILLOT", prenom="Damien", administrateur=False, password=password,
                        dateNaissance=datetime.fromisoformat('2003-09-08'), numEtu=6))


def insertion_Competence():
    """function to populate some competence in database need database.create_all() before excute this"""
    db.session.add(Competence(id=1, competence="PHP"))
    db.session.add(Competence(id=2, competence="SQL"))
    db.session.add(Competence(id=3, competence="HTML/CSS"))
    db.session.add(Competence(id=4, competence="Java"))
    db.session.add(Competence(id=5, competence="Python"))
    db.session.add(Competence(id=6, competence="C++"))


def insertion_Comp():
    """function to populate some company in database need database.create_all() before excute this"""
    db.session.add(Ent(nomEnt="EDF", idEnt=1, priorite=False, blackList=False, _competences=[get_Cpt_by_name("PHP")]))
    db.session.add(Ent(nomEnt="GRDF", idEnt=2, priorite=False, blackList=False, _competences=[get_Cpt_by_name("PHP")]))
    db.session.add(Ent(nomEnt="SNCF", idEnt=3, priorite=False, blackList=False, _competences=[get_Cpt_by_name("PHP")]))
