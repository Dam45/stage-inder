import scrapy 

import json
from bs4 import BeautifulSoup
from urllib.request import urlopen


class FinalSpider(scrapy.Spider): 
  name = "data"

  start_urls = [ 
    "https://fr.wikipedia.org/wiki/Thales",
    "https://fr.wikipedia.org/wiki/Cdiscount",
    "https://fr.wikipedia.org/wiki/Amazon",
    "https://fr.wikipedia.org/wiki/Microsoft",
    "https://fr.wikipedia.org/wiki/IBM",
    "https://fr.wikipedia.org/wiki/LGM_Group",
    "https://fr.wikipedia.org/wiki/Tesla_(automobile)",
    "https://fr.wikipedia.org/wiki/Apple",
    "https://fr.wikipedia.org/wiki/Xiaomi",
    "https://fr.wikipedia.org/wiki/Airbus_Commercial_Aircraft"
   ]

  def parse(self, response):
    page = self.start_urls[0]

    json_file = open("dataFinal.json", "a", encoding='utf8')
    json_file.close()
    
    entreprise = response.css('h1.firstHeading::text').get()
    print(entreprise)
    
    html = urlopen(page)
    
    p1 = response.css('p').getall()
    p = p1[4]
    print(type(p))
    res = BeautifulSoup(p, 'html.parser')
    print(res.get_text())
    description = res.get_text()
    soup = BeautifulSoup(html, 'html.parser')


    liste_scrap = ["Création", "Fondateurs", "Forme juridique", "Slogan", "Effectif", "Site web", "Chiffre d'affaires", "Résultat net"]
    liste_info = list()
    tables = soup.find_all('tr')
    for th in tables:
      info = BeautifulSoup(th.get_text(), 'html.parser')
      if info != "":
        liste_info.append(info.get_text())
    print(liste_info)
    liste_scrap_fin = list()
    for info in liste_info:
      for elem in liste_scrap:
        if elem in info:
          elem_scrap = info.replace("\n", ", ")
          liste_scrap_fin.append(elem_scrap)

    elem_scrap_str = ""
    for elem in liste_scrap_fin:
      elem_scrap_str += elem

    liste_split = elem_scrap_str.split(", ")
    liste_index_pop = list()
    for i in range(len(liste_split)):
      if liste_split[i]=="":
        liste_index_pop.append(i)
      if liste_split[i] in liste_scrap:
        liste_index_pop.append(i)
    liste_index_pop.reverse()
    print(liste_index_pop)
    for indice in liste_index_pop:
      liste_split.pop(indice)
    print(liste_split)

    for i in range(len(liste_split)):
      print(liste_scrap[i], " ---> ", liste_split[i])

    dico = {
      'Entreprise':entreprise,
      'Description':description,
      # 'Date de création':liste_split[0]
    }
    # for i in range(1, len(liste_split)):
    #   dico[liste_scrap[i]] = liste_split[i]
    

    json_string = json.dumps(dico, indent=2, ensure_ascii=False)
    print(json_string)
    json_file = open("dataFinal.json", "a")
    
    json_file.write(json_string)
    json_file.close()



