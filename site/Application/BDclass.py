from flask_login import UserMixin
from sqlalchemy import func, select, update
from .app import db, login_manager

lien_cpt_etu = db.Table('cpt_etu',
                        db.Column("num_etu", db.Integer, db.ForeignKey("user.numEtu")),
                        db.Column("cmpt_id", db.Integer, db.ForeignKey("competence.id")))

lien_cpt_ent = db.Table('cpt_ent',
                        db.Column("idEnt", db.Integer, db.ForeignKey("ent.idEnt")),
                        db.Column("cmpt_id", db.Integer, db.ForeignKey("competence.id")))

proposition = db.Table('prop',
                       db.Column("num_etu", db.Integer, db.ForeignKey("user.numEtu")),
                       db.Column("idEnt", db.Integer, db.ForeignKey("ent.idEnt")),
                       db.Column("like", db.Boolean))


class Ent(db.Model):
    __tablename__ = "ent"
    idEnt = db.Column(db.Integer, primary_key=True, nullable=False)
    nomEnt = db.Column(db.String(80), unique=True, nullable=False)
    priorite = db.Column(db.Boolean, nullable=False)
    blackList = db.Column(db.Boolean, nullable=False)
    desc = db.Column(db.String(300))
    nb_proposition = db.Column(db.Integer, default=0)
    code_ape = db.Column(db.String(5))
    effectifs = db.Column(db.Integer)
    num_siren = db.Column(db.String(9))
    num_siret = db.Column(db.String(14))

    _competences = db.relationship('Competence', secondary=lien_cpt_ent,
                                   backref=db.backref('lien_cpt_ent', lazy='dynamic',
                                                      )
                                   )

    users = db.relationship('User', backref='entreprises', lazy='dynamic', secondary=proposition)

    def __repr__(self):
        return '<Entreprise : %r (%s) %s>' % (self.nomEnt, self.idEnt, self._competences)


class User(db.Model, UserMixin):
    __tablename__ = "user"
    nom = db.Column(db.String(20), nullable=False)
    prenom = db.Column(db.String(20), nullable=False)
    password = db.Column(db.String(20), nullable=False)
    numEtu = db.Column(db.String(20), nullable=False, primary_key=True)
    dateNaissance = db.Column(db.DateTime, nullable=True)
    administrateur = db.Column(db.Boolean, nullable=False)

    _competences = db.relationship('Competence', secondary=lien_cpt_etu,
                                   backref=db.backref('lien_cpt_etu', lazy='dynamic'
                                                      )
                                   )

    _entreprises = db.relationship('Ent', secondary=proposition,
                                   backref=db.backref('proposition', lazy='dynamic'))

    def get_id(self):
        return self.numEtu

    def __repr__(self):
        return '<USER : %s %s %s %s %s %s>' % (
            self.prenom, self.nom, self.password, self.numEtu, self._competences, self._entreprises)


class Competence(db.Model):
    __tablename__ = "competence"
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    competence = db.Column(db.String(80), nullable=False, unique=True)

    entreprises = db.relationship('Ent', backref='competences', lazy='dynamic', secondary=lien_cpt_ent)
    users = db.relationship('User', backref='competences', lazy='dynamic', secondary=lien_cpt_etu,
                            )

    def __repr__(self):
        return '<Competence : %r>' % self.competence


def get_sample_ent_pc():
    return Ent.query.limit(12).all()


def get_sample_ent_tel():
    return Ent.query.limit(1).all()


def get_User(numetu):
    """return User object by is numetu"""
    return User.query.filter(User.numEtu == numetu).first()


def get_Competence(cpt_id):
    """return User object by is numetu"""
    return Competence.query.filter(Competence.id == cpt_id).first()


def get_Ent(ent_id):
    """return User object by is numetu"""
    return Ent.query.filter(Ent.idEnt == ent_id).first()


def get_User_by_name(nom, prenom):
    return db.session.query(User).filter(User.nom == nom and User.prenom == prenom).first()


def get_user_by_etu_num(num_etu):
    return db.session.query(User).filter(User.numEtu == num_etu).first()


def get_Ent_by_name(ent_name):
    return db.session.query(Ent).filter(Ent.nomEnt == ent_name).first()

def get_idEnt_by_name(ent_name):
    return db.session.query(Ent.idEnt).filter(Ent.nomEnt == ent_name).first()

def get_Cpt_by_name(cpt_name):
    """return Competence object by the competence name"""
    cpt = db.session.query(Competence).filter(Competence.competence == cpt_name).first()
    return cpt


def get_nb_proposition(id_ent):
    """returns the number of times the company identified by id_ent has been proposed"""
    ent = get_Ent(id_ent)
    return ent.nb_proposition


def add_Cpt(cpt_name):
    """add the competence to the BD"""
    cpt_name = cpt_name.upper()
    db.session.add(Competence(competence=cpt_name))
    db.session.commit()


def add_Ent(nomEnt, priorite, blackList, desc):
    """add the compagny to the BD"""
    db.session.add(Ent(nomEnt=nomEnt, priorite=priorite, blackList=blackList, desc=desc))
    db.session.commit()


def add_User(nom, prenom, administrateur, password, dateNaissance, numEtu):
    """add the User to the BD"""
    db.session.add(User(nom=nom, prenom=prenom, administrateur=administrateur, password=password,
                        dateNaissance=dateNaissance, numEtu=numEtu))
    db.session.commit()


def add_cpt_to_user(numEtu, id_cpt):
    """add skill to user """
    u = get_User(numEtu)
    u._competences.append(get_Competence(id_cpt))
    db.session.commit()


def del_cpt_to_user(numEtu, id_cpt):
    """add skill to user """
    u = get_User(numEtu)
    u._competences.remove(get_Competence(id_cpt))
    db.session.commit()


def add_cpt_to_ent(id_ent, id_cpt):
    """add skill to user """
    e = get_Ent(id_ent)
    e._competences.append(get_Competence(id_cpt))
    db.session.commit()


def add_nb_proposition(id_ent):
    """add 1 to the number of times the company has been proposed identify by id_ent"""
    ent = get_Ent(id_ent)
    ent.nb_proposition += 1
    db.session.commit()


def purpose_ent_to_user(numEtu, id_ent):
    """add compagny to user //under devllopement TODO//"""
    u = get_User(numEtu)
    u._entreprises.append(get_Ent(id_ent))
    db.session.commit()


def edit_ent(idEnt, nomEnt, priorite, blackList):
    state = db.session.query(Ent).filter(Ent.idEnt == idEnt).update(
        {Ent.nomEnt: nomEnt,
         Ent.priorite: priorite,
         Ent.blackList: blackList})
    db.session.commit()
    return state


def edit_user(numEtu, nom, prenom, administrateur, password, dateNaissance):
    state = db.session.query(User).filter(User.numEtu == numEtu).update(
        {Ent.nom: nom,
         Ent.prenom: prenom,
         Ent.administrateur: administrateur,
         Ent.password: password,
         Ent.dateNaissance: dateNaissance})
    db.session.commit()
    return state


def edit_cpt(id_cpt, cpt_name):
    state = db.session.query(Competence).filter(Competence.id == id_cpt).update(
        {Ent.cpt_name: cpt_name})
    db.session.commit()
    return state


def del_ent(id_ent):
    state = db.session.query(Ent).filter(Ent.idEnt == id_ent).delete(synchronize_session=False)
    db.session.commit()
    return state


def del_user(numEtu):
    user = User.query.filter(User.numEtu == numEtu).first()
    if not user.administrateur:
        state = db.session.query(User).filter(User.numEtu == numEtu).delete(synchronize_session=False)
        db.session.commit()
        return state
    raise Exception("This user is an administrator") 


def del_cpt(id_cpt):
    state = db.session.query(Competence).filter(Competence.id == id_cpt).delete(synchronize_session=False)
    db.session.commit()
    return state


def create_user(numEtu, nom, prenom, administrateur, password, dateNaissance):
    user = User(numEtu=numEtu, nom=nom, prenom=prenom, administrateur=administrateur, password=password,
                dateNaissance=dateNaissance)
    state = db.session.add(user)
    db.session.commit()
    return user

def create_ent(nomEnt, priorite, blackList, desc, code_ape, effectifs, num_siren, num_siret):
    """add a compagny to the BD"""
    db.session.add(Ent(nomEnt=nomEnt, priorite=priorite, blackList=blackList, desc=desc))
    db.session.commit()


def get_all_ent():
    return Ent.query.all()


def get_all_user():
    return User.query.all()


def get_all_comp():
    return Competence.query.all()


def reset_nb_proposition_ent(id_ent):
    """reset the num"""
    ent = get_Ent(id_ent)
    ent.nb_proposition = 0
    db.session.commit()


def reset_nb_proposition_all_ent():
    """reset the num"""
    compagnies = get_all_ent()
    for compagny in compagnies:
        compagny.nb_proposition = 0
    db.session.commit()

def get_avg_comp_by_etu():
    cpt = 0
    data = db.session.query(User._competences).all()
    users = db.session.query(User.numEtu).all()
    if data and len(data)>0:
        cpt = sum(map(lambda x: x[0], data))
        return format(cpt/len(users), '1.0f')
    return 0

def get_avg_comp_by_ent():
    cpt = 0
    data = db.session.query(Ent._competences).all()
    ents = db.session.query(Ent.idEnt).all()
    if data and len(data)>0:
        cpt = sum(map(lambda x: x[0], data))
        return cpt/len(ents)
    return 0
    # return format(cpt/len(ents), '1.0f')

def get_avg_like():
    data = db.session.query(User._entreprises).all()
    if data and len(data)>0:
        users = db.session.query(User.numEtu).all()
        return len(data)/len(user)
    return 0


def blackList_ent(id,value):
    """
        set the blacklist value of the company to true or false
    """
    state = db.session.query(Ent).filter(Ent.idEnt == id).update({Ent.blackList: value})
    db.session.commit()
    ent = db.session.query(Ent).filter(Ent.idEnt == id).first()
    print(ent.blackList)
    return state
    

def setAdmin(id,value):
    """
        set the admin value of the User to true or false
    """
    state = db.session.query(User).filter(User.numEtu == id).update({User.administrateur: value})
    db.session.commit()
    return state

@login_manager.user_loader
def load_user(numEtu):
    return User.query.get(numEtu)

def get_idEnt_by_name(ent_name):
    return db.session.query(Ent.idEnt).filter(Ent.nomEnt == ent_name).first()

def get_like(id_etu):
    res = []
    propositions = select(proposition).where((proposition.c.num_etu == id_etu))
    rs = db.engine.connect().execute(propositions)
    for row in rs:
        if(row not in res):
            res.append(row)
    return res

def set_like(id_etu,idEnt,like):
    up=update(proposition).where((proposition.c.num_etu == id_etu) & (proposition.c.idEnt == idEnt)).values(like=like)
    db.engine.connect().execute(up)
    return 0