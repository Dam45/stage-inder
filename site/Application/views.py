from ast import Pass
from calendar import Calendar
from os import name
from re import M
from tokenize import String
from flask_wtf import FlaskForm
from sqlalchemy import false, null
from wtforms import StringField, HiddenField, PasswordField, DateField, validators, BooleanField, TextAreaField
from wtforms.fields.numeric import IntegerField
from wtforms.validators import DataRequired, Email
from .app import app, loginURL, registerURL, homeURL, db, profileURL
from .BDclass import *
from flask import jsonify, render_template, request, redirect, url_for, flash, session
from flask_login import login_user, current_user, logout_user
from hashlib import sha256
from functools import wraps
import json
# from flask import g 


# from werkzeug.security import generate_password_hash

class RegisterForm(FlaskForm):
    etu_num = IntegerField("Numéro étudiant", validators=[validators.DataRequired(message="Veuillez remplir ce champ.")])
    name = StringField("Nom", validators=[validators.DataRequired(message="Veuillez remplir ce champ.")])
    first_name = StringField("Prénom", validators=[validators.DataRequired(message="Veuillez remplir ce champ.")])
    mail = StringField("Adresse mail", validators=[validators.DataRequired(message="Veuillez remplir ce champ.")])
    password = PasswordField("Mot de passe", [
        validators.DataRequired(message="Veuillez remplir ce champ."),
        validators.EqualTo("confirm", message="Mot de passe incorrect")
    ])
    confirm = PasswordField("Confirmer mot de passe",
                            validators=[validators.DataRequired(message="Veuillez remplir ce champ.")])
    birthday = DateField('Start Date', format='%m/%d/%Y')
    admin = BooleanField("Administrateur")

    def user_register(self):
        m = sha256()
        m.update(self.password.data.encode())
        password = m.hexdigest()
        u = create_user(self.etu_num.data, self.name.data, self.first_name.data, self.admin.data, password, self.birthday.data)
        print(u)


class LoginForm(FlaskForm):
    etu_num = IntegerField("Numéro étudiant", validators=[validators.DataRequired(message="Veuillez remplir ce champ.")])
    password = PasswordField("Mot de passe", validators=[validators.DataRequired("Veuillez remplir ce champ.")])

    def get_authenticated_user(self):
        user = get_user_by_etu_num(self.etu_num.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        password = m.hexdigest()
        return user if password == user.password else None

class CompRegisterForm(FlaskForm):
    nom_comp = StringField(" Nom de la compétence")

    def comp_register(self):
        add_Cpt(self.nom_comp.data)


class EntRegisterForm(FlaskForm):
    validator = [validators.DataRequired(message="Veuillez remplir ce champ.")]
    name = StringField("Nom", validators=validator)
    # priorite = BooleanField("Prioritaire")
    black_List = BooleanField("BlackList")
    desc = TextAreaField("description")
    code_ape = StringField("Code APE",validators=validator)
    effectifs = IntegerField("Effectifs")
    num_siren = IntegerField("Numéro SIREN",validators=validator)
    num_siret = IntegerField("Numéro SIRET",validators=validator)
    

    def ent_register(self):
        create_ent(self.name.data, False, self.black_List.data, self.desc.data, self.code_ape.data, 
            self.effectifs.data, self.num_siren.data, self.num_siret.data)



def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_user.is_authenticated:
            flash("vous devez vous connecter")
            return redirect(url_for('login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not current_user.is_authenticated:
            return redirect(url_for('login', next=request.url))
        elif not current_user.administrateur:
            return redirect(url_for('notFound',next=request.url))
        return f(*args, **kwargs)
    return decorated_function


@app.route("/")
def homePage():
    if request.method == 'POST':
        return None
    else:
        if not current_user.is_authenticated:
            return render_template("home.html", login=loginURL, register=registerURL, home=homeURL,
            title="Accueil",isLogged=current_user.is_authenticated)
        else:
            return render_template("home.html", login=loginURL, register=registerURL, home=homeURL,
            title="Accueil",isLogged=current_user.is_authenticated, isAdmin=current_user.administrateur)


@app.route("/Inscription", methods=['GET', 'POST'])
def register():
    f = RegisterForm()
    if request.method == 'POST':
        f.user_register()
        return redirect(url_for('login'))
    if f.validate_on_submit():
        print("Inscription")
        return redirect(url_for('login'))
    if not current_user.is_authenticated:
        return render_template("register.html", login=loginURL, register=registerURL, home=homeURL, title="Inscription",
            form=f, profile=profileURL,isLogged=current_user.is_authenticated)
    else:
        return render_template("register.html", login=loginURL, register=registerURL, home=homeURL, title="Inscription",
            form=f, profile=profileURL,isLogged=current_user.is_authenticated,isAdmin=current_user.administrateur)


@app.route("/Connexion", methods=['GET', 'POST'])
def login():
    user = None
    f = LoginForm()
    if f.validate_on_submit():
        user = f.get_authenticated_user()
    if user:
        login_user(user)
        flash("Connexion réussi avec succès !")
        return redirect(url_for('stages'))
    if not current_user.is_authenticated:
        return render_template("login.html", form=f, title="Connexion",isLogged=current_user.is_authenticated)
    else :
        return render_template("login.html", form=f, title="Connexion",isLogged=current_user.is_authenticated, 
            isAdmin=current_user.administrateur)


@app.route("/Déconnexion", methods=['GET','POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('homePage'))


@app.route("/Contact", methods=['GET', 'POST'])
def contact():
    if not current_user.is_authenticated:
        return render_template("contact.html", title="Contact",isLogged=current_user.is_authenticated)
    else :
        return render_template("contact.html", title="Contact",isLogged=current_user.is_authenticated, 
            isAdmin=current_user.administrateur)


@app.route("/Settings", methods=['GET', 'POST'])
def settings():
    if not current_user.is_authenticated:
        return render_template("settings.html", title="Paramètres",isLogged=current_user.is_authenticated)
    else :
        return render_template("settings.html", title="Paramètres",isLogged=current_user.is_authenticated, 
            isAdmin=current_user.administrateur)


@app.route("/Historique", methods=['GET', 'POST'])
@login_required
def historique():
    page = request.args.get('page', 1, type=int)
    # entreprises = rechercheEntreprises()
    entreprises = Ent.query.paginate(page=page, per_page=10, error_out=True)
    print("------------------------------------", entreprises.items)
    return render_template("history.html", title="Historique", isLogged=current_user.is_authenticated, entreprises=entreprises.items)


@app.route("/Admin", methods=['GET', 'POST'])
@admin_required
def admin():
    ents = get_all_ent()
    users = get_all_user()
    cpts = get_all_comp()
    avgCptEtu = get_avg_comp_by_etu()
    avgCptEnt = get_avg_comp_by_ent()
    avgLike = get_avg_like()
    return render_template("admin.html", title="Admin",isLogged=current_user.is_authenticated, isAdmin=current_user.administrateur, 
        ents = ents,users = users, cpts = cpts, avgCptEtu = avgCptEtu, avgCptEnt=avgCptEnt , avgLike = avgLike)


@app.route("/Admin/NewUser", methods=['GET', 'POST'])
@admin_required
def newUser():
    f = RegisterForm()
    if request.method == 'POST':
        f.user_register()
        return redirect(url_for('admin'))
    # if f.validate_on_submit():
    #     print("Inscription")
    #     return redirect(url_for('login'))
    return render_template("newUser.html", login=loginURL, register=registerURL, home=homeURL, title="Ajout utilisateur",
        form=f, profile=profileURL,isLogged=current_user.is_authenticated,isAdmin=current_user.administrateur)

@app.route("/Admin/NewEnt", methods=['GET', 'POST'])
@admin_required
def newEnt():
    f = EntRegisterForm()
    if request.method == 'POST':
        f.ent_register()
        return redirect(url_for('admin'))
    # if f.validate_on_submit():
    #     print("Inscription")
    #     return redirect(url_for('login'))
    return render_template("newEnt.html", login=loginURL, register=registerURL, home=homeURL, title="Ajout entreprise",
        form=f, profile=profileURL,isLogged=current_user.is_authenticated,isAdmin=current_user.administrateur)

@app.route("/Admin/NewComp", methods=['GET', 'POST'])
@admin_required
def newComp():
    f = CompRegisterForm()
    if request.method == 'POST':
        f.comp_register()
        return redirect(url_for('admin'))
    # if f.validate_on_submit():
    #     print("Inscription")
    #     return redirect(url_for('login'))
    return render_template("newComp.html", login=loginURL, register=registerURL, home=homeURL, title="Ajout compétence",
        form=f, profile=profileURL,isLogged=current_user.is_authenticated,isAdmin=current_user.administrateur)


@app.route("/Admin/DeleteEnt/<int:id>", methods=['POST'])
@admin_required
def delEnt(id):
    status = del_ent(id)
    return str(status)


@app.route("/Admin/DeleteEtu/<int:id>", methods=['POST'])
@admin_required
def delEtu(id):
    status =del_user(id)
    return str(status)


@app.route("/Admin/DeleteComp/<int:id>", methods=['POST'])
@admin_required
def delComp(id):
    status = del_cpt(id)
    return  str(status)


@app.route("/Admin/SauverEnt", methods=["POST"])
# @admin_required
def SauverEnt():
    liste = json.loads(request.form.get('BlackListData'))
    # print("Liste :")
    # print(liste)
    for x in liste:
        blackList_ent(x[0],x[1])
        # print("ENT :"+x[0]+str(x[1]))
    return "0";

@app.route("/Admin/SauverEtu", methods=["POST"])
# @admin_required
def SauverEtu():
    liste = json.loads(request.form.get('AdminData'))
    # print("Liste :")
    # print(liste)
    for x in liste:
        setAdmin(x[0],x[1])
        # print("User :"+x[0]+" "+str(x[1]))
    return "0";
    




# @app.route("/Admin/Entreprise/<int:id>", methods=['GET', 'POST'])
# @admin_required
# def admin_ent(id):
#     ent = get_Ent(id)
#     if request.method == 'POST':
#         # f.edit_ent(idEnt, nomEnt, priorite, blackList)
#         return redirect(url_for('admin_ent',id = id))
#     return render_template("newUser.html", login=loginURL, register=registerURL, home=homeURL, title=ent.nomEnt, 
#         profile=profileURL,isLogged=current_user.is_authenticated,isAdmin=current_user.administrateur)


#----------------------------------------------------------------------------------------------------------------------------------
# Cette fonction va récupérer les compétences de l'utilisateur actuel et les envoyer au template profil.html qui va les afficher
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Profil", methods=['GET'])
@login_required
def profil():
    utilisateur = get_User(current_user.numEtu)
    competencesDispo=get_all_comp()
    return render_template("profil.html", title="Profil",isLogged=current_user.is_authenticated, isAdmin=current_user.administrateur, 
        nom=utilisateur.nom, prenom=utilisateur.prenom, numetu=utilisateur.numEtu, competences=utilisateur._competences, 
        competences_dispo=competencesDispo)

#----------------------------------------------------------------------------------------------------------------------------------
# Cette fonction va être appelée après avoir reçu un retour POST par profil() et va permettre de mettre à jour les
# compétences de l'utililsateur actuel dans la base de donnée, puis de rafraichir la page en rappelant profil
#----------------------------------------------------------------------------------------------------------------------------------

@app.route("/Profil", methods=['POST'])
@login_required
def profilrecup():
    nouvelleDonnee=request.form
    competencesDispo=get_all_comp()
    for comp in competencesDispo:
        if comp.competence in nouvelleDonnee:
            if(comp not in get_User(current_user.numEtu)._competences):
                add_cpt_to_user(current_user.numEtu,comp.id)
        else:
            if(comp in get_User(current_user.numEtu)._competences):
                del_cpt_to_user(current_user.numEtu,comp.id)
    return profil()

@app.route("/Stages", methods=['GET'])
@login_required
def stages():
    entreprises=rechercheEntreprises()
    return render_template("swipeComp.html", title="Entreprises",isLogged=current_user.is_authenticated, 
        isAdmin=current_user.administrateur,entreprises=entreprises)
        

#----------------------------------------------------------------------------------------------------------------------------------
# Cette fonction va être appelée via le JS des cartes d'entreprises, avec les données du nom de l'entreprise
# avec l'argument 'nameEnt' et la réponse de l'étudiant face à cette entreprise via l'argument 'likeEnt'
# qui vaudra 'true' ou 'false'.
# Cette fonction permettra via les donnée précisées avant de savoir la réponse de l'étudiant face à l'entreprise
# en parametre de la donnée js et de mettre à jour la BD en fonction
#----------------------------------------------------------------------------------------------------------------------------------

@app.route('/recupStages', methods=['POST'])
def recupStages():
    nameEnt = request.form.get('nameEnt')
    likeEnt = request.form.get('likeEnt')
    idEnt = get_idEnt_by_name(nameEnt)
    numEtu = current_user.numEtu
    # if(likeEnt == "true"):
        # purpose_ent_to_user(numEtu, idEnt, True)
    # else:
        # purpose_ent_to_user(numEtu, idEnt, False)
    return ""

@app.route("/404",methods=['GET','POST'])
def notFound():
    return render_template("404.html",title="404",isLogged=current_user.is_authenticated, isAdmin=current_user.administrateur)

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre de calculer le nombre de points communs entre l'entreprise présenté et l'étudiant
#----------------------------------------------------------------------------------------------------------------------------------
def pointsCommunEntreprise(nomEnt):
    cpt = 0
    for comp in get_Ent_by_name(nomEnt)._competences:
        if comp in current_user._competences:
            cpt+=1
    return cpt

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre de dire si une entreprise est apte ou non à être présenté à cet étudiant c'est à dire si elle est
# dans la blacklist ou non si le nombre de candidat proposé est > 10 ou si à déjà été présenté à cet étudiant
#----------------------------------------------------------------------------------------------------------------------------------
def matchEntrepriseUser(nomEnt):
    entreprise = get_Ent_by_name(nomEnt)
    if entreprise in current_user._entreprises or entreprise.nb_proposition > 10 or entreprise.blackList == True:
        return False
    return True

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre via l'argument passé par rechercheEntreprises qui est une liste de nom d'entreprises dont l'on
# va prendre les 9 premiers , où l'on va prendre l'objet entreprise grâce à son nom et de les ajouter à une nouvelle liste
# et utiliser pour chaque entreprise la fonction purpose_ent_to_user qui va faire en sorte que l'on ne reproposera pas l'entreprise
# à cet étudiant
#----------------------------------------------------------------------------------------------------------------------------------
def retourne9Entreprises(listeNomEntre):
    """

    @param listeNomEntre:
    @type listeNomEntre:
    @return:return a list of 9 companies
    @rtype:list(ent)
    """
    retour9entreprises=[]
    for nom in listeNomEntre:
        if len(retour9entreprises) < 9:
            entreprise = get_Ent_by_name(nom)
            retour9entreprises.append(entreprise)
    return retour9entreprises

#----------------------------------------------------------------------------------------------------------------------------------
# Fonction qui va permettre via les fonctions pointsCommunEntreprise / matchEntrepriseUser et retourne9Entreprise
# de retourner une liste des 9 entreprises qui convienne le mieux à l'étudiant connecté grâce à ses conptences par ordre croissant
# et de filtrer les entreprises étant dans la black list ayant + de 10 candidats de proposés et celles qui ont déjà été proposé
# à l'étudiant via la fonction matchEntrepriseUser
#----------------------------------------------------------------------------------------------------------------------------------
def rechercheEntreprises():
    """
    Function that will allow with the "pointsCommunEntreprise" / "matchEntrepriseUser" and "retourne9Entreprise" functions
    to return a list of the 9 companies that best suit to the connected student thanks to his skills in ascending order.
    This function also filter the companies being in the black list, those having more than 10 candidates proposed and those
    which have already been proposed to the student with the "matchEntrepriseUser" function.

    @return:return a list of 9 companies
    @rtype:list(Ent)
    """
    dictEntreprises=dict()  
    for ent in get_all_ent():
        if matchEntrepriseUser(ent.nomEnt):
            dictEntreprises[ent.nomEnt] = (pointsCommunEntreprise(ent.nomEnt),ent.priorite)
    res=sorted(dictEntreprises,key=lambda key: (dictEntreprises[key][0],dictEntreprises[key][1]), reverse=True)
    retour=list(reversed(retourne9Entreprises(res)))
    return retour