from flask import Flask
import os.path
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bootstrap import Bootstrap

app = Flask(__name__)
app.config.from_object(__name__)
app.config["SECRET_KEY"] = "74cb927d-a747-4778-a1f5-0a25e378ddf3"

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)

login_manager = LoginManager(app)
loginURL = "/login"
registerURL = "/register"
profileURL = "/profile"
homeURL = "/"

Bootstrap(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True

def mkpath(p):
   return os.path.normpath(os.path.join(os.path.dirname(__file__), p))

if __name__ == "__init__":
   app.run(debug=True)